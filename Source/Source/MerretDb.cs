﻿using System.Configuration;
using System.Data;
using System.Data.Odbc;

namespace Source
{
    internal static class MerretDb
    {
        private static string _cs =
            ConfigurationManager.ConnectionStrings["merretDb"].ConnectionString;

        internal static DataTable ExecuteQuery(string tableName, string sql)
        {
            DataTable dt = new DataTable(tableName);

            using (OdbcConnection con = new OdbcConnection(_cs))
            {
                con.Open();

                using (OdbcCommand cmd = new OdbcCommand(sql, con))
                {
                    using (OdbcDataAdapter da = new OdbcDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }
                }
            }

            return dt;
        }

        internal static object ExecuteScalar(string sql)
        {
            object result = null;

            var dt = ExecuteQuery("TmpData", sql);

            if (dt.Rows.Count > 0)
                result = dt.Rows[0].ItemArray[0];

            return result;
        }
    }
}
