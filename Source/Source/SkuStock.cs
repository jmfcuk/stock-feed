﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;

namespace Source
{
    public class SkuStock
    {
        public string SiteId { get; set; }
        public string Sku { get; set; }
        public long Available { get; set; }

        public static SkuStockList FromDataTable(DataTable dt)
        {
            SkuStockList ssl = new SkuStockList();

            foreach (DataRow dr in dt.Rows)
            {
                SkuStock ss = new SkuStock();
                ss.SiteId = dr["SITEID"].ToString();
                ss.Sku = dr["SKU"].ToString();
                ss.Available = long.Parse(dr["AVAILABLE"].ToString());

                ssl.Add(ss);
            }

            return ssl;
        }
    }

    public class SkuStockList : List<SkuStock>
    {
        public string Serialize()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    // sku to stock map.
    public class SkuStockDictionary : Dictionary<string, long> { }
}
