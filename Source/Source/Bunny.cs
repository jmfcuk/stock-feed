﻿using RabbitMQ.Client;
using System;
using System.Configuration;
using System.Text;

namespace Source
{
    internal class Bunny
    {
        private readonly string _exchange = null;
        private readonly string _exchangeType = null;

        internal Bunny(string exchange, string exchangeType)
        {
            _exchange = exchange;
            _exchangeType = exchangeType;
        }

        internal void Publish(string topic, string msg)
        {
            ConnectionFactory factory = GetConnectionFactory();

            using (IConnection connection = factory.CreateConnection())
            using (IModel channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: _exchange,
                                        type: _exchangeType);

                byte[] body = Encoding.UTF8.GetBytes(msg);

                channel.BasicPublish(exchange: _exchange,
                                     routingKey: topic,
                                     basicProperties: null,
                                     body: body);

                Console.WriteLine("Sent '{0}':'{1}'", topic, msg);
            }
        }

        private static ConnectionFactory GetConnectionFactory()
        {
            ConnectionFactory cf = new ConnectionFactory();
            cf.HostName = ConfigurationManager.AppSettings["rmqHost"];
            cf.UserName = ConfigurationManager.AppSettings["rmqUid"];
            cf.Password = ConfigurationManager.AppSettings["rmqPwd"];
            cf.Port = AmqpTcpEndpoint.UseDefaultPort; // 5672

            return cf;
        }
    }
}

