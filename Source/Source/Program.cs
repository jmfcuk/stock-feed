﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Timers;

namespace Source
{
    class Program
    {
        private static readonly string _merretSchema = 
            ConfigurationManager.AppSettings["merretSchema"];

        // Expand this to be a list.
        private static readonly string _topic =
            ConfigurationManager.AppSettings["topics"];

        private static TopicMap _topicMap = new TopicMap();

        private static string _sql =
            "SELECT pssite As SiteId, pssku AS SKU, " +
            "psunit - psrunt - psotun - psdama - pswhrs - psfrzn - psqcqu - psqcck - psaqty AS Available " +
            $"FROM {_merretSchema}.PDPRSIP WHERE PSCOMP = 1 AND psunit > 0 " + //AND PSSKU IN (2522508) " +
            $"AND PSSITE = '{_topicMap[_topic]}'";

        private static string _cs =
            ConfigurationManager.ConnectionStrings["merretDb"].ConnectionString;

        private static Timer _timer;

        private static Bunny _bunny = 
            new Bunny(ConfigurationManager.AppSettings["exchange"],
                      ConfigurationManager.AppSettings["exchangeType"]);
        
        static void Main(string[] args)
        {
            _timer = new Timer();
            _timer.Interval = 5000D;
            _timer.Elapsed += OnTimer;

            _timer.Start();

            Console.WriteLine("Enter to exit...");
            Console.ReadLine();
        }

        private static void OnTimer(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();

            DataTable dt = MerretDb.ExecuteQuery("Stock", _sql);

            SkuStockList ssl = SkuStock.FromDataTable(dt);
            string json = ssl.Serialize();

            _bunny.Publish(_topic, json);
            
            _timer.Start();
        }

        private class TopicMap : Dictionary<string, string>
        {
            internal TopicMap()
            {
                this.Add("stock.knightsbridge", "0001");
            }
        }
    }
}

