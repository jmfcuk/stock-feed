﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Configuration;
using System.Text;

namespace Sub
{
    class Program
    {
        private const string _EXCHANGE = "JOHNS_EXCHANGE";
        private const string _EXCHANGE_TYPE = "topic";

        static void Main(string[] args)
        {
            if (args == null || args.Length < 1)
            {
                PrintUsage();
                Environment.ExitCode = 0;
                return;
            }

            ConnectionFactory factory = GetConnectionFactory();

            using (IConnection connection = factory.CreateConnection())
            using (IModel channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: _EXCHANGE, type: _EXCHANGE_TYPE);

                string queueName = channel.QueueDeclare().QueueName;

                foreach (string bindingKey in args)
                {
                    channel.QueueBind(queue: queueName,
                                      exchange: _EXCHANGE,
                                      routingKey: bindingKey);
                }

                Console.WriteLine("Waiting for messages. To exit press CTRL+C");

                EventingBasicConsumer consumer = new EventingBasicConsumer(channel);

                consumer.Received += (model, ea) =>
                {
                    byte[] body = ea.Body;
                    string message = Encoding.UTF8.GetString(body);
                    string routingKey = ea.RoutingKey;
                    Console.WriteLine("Received '{0}':'{1}'",
                                      "", "");// routingKey,
                                      //message);
                };

                channel.BasicConsume(queue: queueName,
                                     autoAck: true,
                                     consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }

        private static ConnectionFactory GetConnectionFactory()
        {
            ConnectionFactory cf = new ConnectionFactory();
            cf.HostName = ConfigurationManager.AppSettings["rmqHost"];
            cf.UserName = ConfigurationManager.AppSettings["rmqUid"];
            cf.Password = ConfigurationManager.AppSettings["rmqPwd"];
            cf.Port = AmqpTcpEndpoint.UseDefaultPort; // 5672

            return cf;
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("Sub <topic> <topic>...");
        }
    }
}

