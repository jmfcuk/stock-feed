var amqp = require('amqplib/callback_api');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);


var args = process.argv.slice(2);

const topic = 'stock.knightsbridge';

if (args.length == 0) {
  console.log("Usage: index <topic>");
  process.exit(0);
}

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  console.log('user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

amqp.connect('amqp://admin:admin@hnap70', function(err, conn) {

  conn.createChannel(function(err, ch) {

    var ex = 'JOHNS_EXCHANGE';

    ch.assertExchange(ex, 'topic', {durable: false});

    ch.assertQueue('', {exclusive: true}, function(err, q) {

      console.log('q = %s', q.queue);

      console.log('[*] Waiting for messages. To exit press CTRL+C');

      const topics = args.slice(1);

      topics.forEach(function(key) {
        console.log('bind topic to queue: %s -> %s', q.queue, key);
        ch.bindQueue(q.queue, ex, key);
      });

      ch.consume(q.queue, function(msg) {

        console.log("[x] %s: '%s'", msg.fields.routingKey, msg.content.toString());

        io.emit('stock-message', msg);

      }, {noAck: true});
    });
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

// io.on('connection', function(socket){
//   // socket.on('chat message', function(msg){
//   //   console.log('chat message: %s', msg);
//   //   io.emit('stock-msg', msg);
//   // });
// });

// http.listen(3000, function(){
//   console.log('listening on *:3000');
// });


